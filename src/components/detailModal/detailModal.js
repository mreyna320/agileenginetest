import React from 'react';
import './detailModal.css';

const detailModal = (props) => {
    const { author, camera, tags, full_picture } = { ...props };

    return (
        <div className="DetailModal" >
            <div className="container">
                <img src={full_picture}></img>
                <div className="overlay">
                    <div className="overlayText">
                        Author: {author} <br />
                    Camera: {camera} <br />
                    hastags: {tags}
                    </div>
                </div>
            </div>
            <div className="buttonBox">
                <div className="floatingButton" onClick={props.sharePicture}>Share</div>
                <div className="floatingButton" onClick={props.closeModal}>Close</div>
            </div>
        </div>
    )

}

export default detailModal;