import React from 'react';
import "./pictureContainer.css";

const pictureContainer = (props) => {
    const { id, cropped_picture, pictureClick } = { ...props };

    return (
        <div className="PictureContainer" onClick={pictureClick}>
            <img src={cropped_picture}></img>
        </div>
    )
}

export default pictureContainer;