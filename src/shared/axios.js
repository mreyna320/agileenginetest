import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://interview.agileengine.com/'
});

instance.interceptors.request.use(
    req => {
        if (instance.defaults.headers.common["Authorization"] || req.url.includes("auth"))
            return req;
        // throw ({ message: "the token is not available" });
    }, error => {
        return Promise.reject(error);
    }
);

export const setAuthToken = token => {
    if (token) {
        //applying token
        instance.defaults.headers.common['Authorization'] = token;
    } else {
        //deleting the token from header
        delete instance.defaults.headers.common['Authorization'];
    }
}

export default instance;