import React, { Component } from 'react';
import { Container, Row, Col } from 'react-grid';
import './App.css';
import axios, { setAuthToken } from './shared/axios';

import PictureContainer from './components/pictureContainer/pictureContainer';
import DetailModal from './components/detailModal/detailModal';

class App extends Component {

  state = {
    page: 1,
    pageCount: null,
    hasMore: true,
    pictures: [],
    pictureDetails: {},
    modalDetailId: null
  }

  componentDidMount() {
    this.loadPage();
  }

  async loadPage() {
    await this.getToken();
    this.getImages();
  }

  getToken() {
    return axios
      .post("auth", { "apiKey": "23567b218376f79d9415" })
      .then(resp => { setAuthToken(resp.data.token); })
  }

  getImages(page) {
    let url = "images";
    if (page)
      url += `?page=${page}`;

    axios
      .get(url)
      .then(
        r => {
          const updPictures = [];
          updPictures.push(...this.state.pictures, r.data.pictures);
          this.setState({ pictures: updPictures, hasMore: r.data.hasMore, pageCount: r.data.pageCount });
        }
      )
  }

  openDetailModal(id) {

    if (this.state.pictureDetails[id]) {
      this.setState({ modalDetailId: id });
      return;
    }

    const url = `images/${id}`;
    axios
      .get(url)
      .then(
        r => {
          const pictureDetailsUpd = { ...this.state.pictureDetails };
          pictureDetailsUpd[id] = r.data;
          this.setState({ modalDetailId: id, pictureDetails: pictureDetailsUpd });
        }
      )
  }

  closeModalHandler() {
    this.setState({ modalDetailId: null });
  }

  render() {

    let pictures;
    if (this.state.pictures.length) {
      pictures = this.state.pictures[this.state.page - 1].map(pic => <Col key={pic.id}><PictureContainer {...pic} pictureClick={() => this.openDetailModal(pic.id)} /></Col>);
    }

    let modal;
    if (this.state.modalDetailId) {
      const picDetails = this.state.pictureDetails[this.state.modalDetailId];
      modal = <DetailModal {...picDetails} closeModal={() => this.closeModalHandler()} />
    }

    return (
      <div>
        <h3>Pictures</h3>
        {modal}
        <Container>
          <Row>
            {pictures}
          </Row>
        </Container>
        <div>
          <div>Prev</div>
          <div>{this.state.page}/{this.state.pageCount} </div>
          <div>Next</div>
        </div>
      </div>
    )
  }

}


export default App;
